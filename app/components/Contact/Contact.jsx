import React, { Component } from 'react'

import style from './Contact.scss'

class Contact extends Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

        this.state = {
            success: false
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value }, () => console.log(this.state))
    }

    handleSubmit(e) {
        e.preventDefault()

        fetch('send.php', {
            method: 'POST'
        })
            .then(response => {
                this.setState({ success: true })
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div className="container">
                <div className="flex-container">
                    <div className="padding-right">
                        <div className="title-container">
                            <h3>About</h3>
                            <hr />
                        </div>

                        <div>
                            <p>
                                Thanks for stopping by, please feel free to get in contact should
                                you have any questions, work requests or just to say hi.
                            </p>

                            <p>
                                I specialise in front end development, ranging from bespoke wordpress
                                builds, single page apps, to full scale CMS builds in JavaScript. I
                                am capable of planning, building, maintaining and seeing a product
                                through the full life cycle.
                            </p>

                            <p>
                                I have over 5 years commercial experience, much longer as a hobby and
                                have worked in a range of different team sizes, mostly using a Agile
                                methodology.
                            </p>

                            <p>
                                I aim to make all my development work as effective and efficient as
                                possible, whilst using the best technologies for the job and keeping
                                up to date with the latest tech trends.
                            </p>
                        </div>
                    </div>

                    <div className="contact-container">
                        <div className={`contact-info ${this.state.success ? 'active' : ''}`}>
                            Thanks for your message, i will get back to you as soon as possible.
                        </div>

                        <div className="title-container">
                            <h3>Contact</h3>
                            <hr />
                        </div>

                        <div className="flex-container">
                            <form onSubmit={this.handleSubmit}>
                                <input
                                    type="text"
                                    placeholder="Name"
                                    name="name"
                                    onChange={this.onChange}
                                    required
                                />

                                <input
                                    type="email"
                                    placeholder="Email"
                                    name="email"
                                    onChange={this.onChange}
                                    required
                                />

                                <input
                                    type="number"
                                    placeholder="Phone Number"
                                    name="number"
                                    onChange={this.onChange}
                                    required
                                />

                                <textarea
                                    placeholder="Message"
                                    name="message"
                                    onChange={this.onChange}
                                    required
                                />

                                <input type="submit" className="button" value="Send" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact
