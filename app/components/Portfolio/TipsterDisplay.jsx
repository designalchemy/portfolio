import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const TipsterDisplay = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Tipster Display</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/tipsterDisplay/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/tipsterDisplay/2.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterDisplay/3.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterDisplay/4.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterDisplay/5.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterDisplay/6.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Tipster Display
                </p>

                <p>
                    <strong>Client:</strong> Racing Post via{' '}
                    <a href="http://www.korelogic.co.uk">Korelogic</a>
                </p>

                <p>
                    <strong>Role:</strong> Lead Developer + project corodination
                </p>

                <p>
                    <strong>Tech used:</strong> React.js, Mobx, Horzion.js, Webpack, HTML, SCSS, JS
                </p>

                <p>
                    <strong>About:</strong> Tipster was a project were i was the sole developer,
                    while working on this projet for RacingPost, i planned the architecture,
                    implmentation and build of this project.
                </p>

                <p>
                    This product is a display that runs on screens in betting shops offering betting
                    tips and odds for the days events. It cycles thought the different events for
                    multipul sports including horse racing, grayhound racing and football.
                </p>

                <p>
                    The data was provided by Racing Posts APIs which then was noramilised and stored
                    in our database with a websocket connection so the stats and info are allways
                    live. Each of these screens could be configured with different branding,
                    different data sources and content.
                </p>

                <p>
                    One of the big challanges were ensuring the product still worked if the internet
                    dropped out would always provide live or the most accurate data.
                </p>

                <p>
                    Another challanage was to ensure it worked on IE10, which with the modern
                    technology stack we are using was difficult.
                </p>

                <p>
                    The various betting tips and info is all sorted by its strength and if the
                    contedant was a active participant in the sporting event.
                </p>

                <p>
                    The tipster was also under a authentication page, meaning it can only be
                    accessed by computers that had been authorised or IP white listed.
                </p>
            </div>
        </div>
    </div>
)

export default TipsterDisplay
