import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const TipsterBackOffice = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Tipster Backoffice</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/tipsterBackoffice/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/tipsterBackoffice/2.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterBackoffice/3.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterBackoffice/4.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterBackoffice/5.jpg" alt="" />
                    </div>

                    <div>
                        <img src="/assets/tipsterBackoffice/6.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Tipster Backoffice
                </p>

                <p>
                    <strong>Client:</strong> Racing Post via{' '}
                    <a href="http://www.korelogic.co.uk">Korelogic</a>
                </p>

                <p>
                    <strong>Role:</strong> Lead Developer + project corodination
                </p>

                <p>
                    <strong>Tech used:</strong> React.js, Mobx, Rethink, Express.js, Webpack, HTML,
                    LESS, JS, AntD
                </p>

                <p>
                    <strong>About:</strong> This project for Racing Post is the CMS for the Tipster
                    display screens. I was lead developer on this product and responsible for nearly
                    all the dev work including API's, database structure, front end and data
                    fetching scripts.
                </p>

                <p>
                    Here its possible to configure the displays and autheticate the hardware the
                    displays are accessed on. This product has mutlipul REST api's that were created
                    to manage the database and front end data.
                </p>

                <p>
                    Real time websocket connection status as well as bandwidth tracking are
                    intergrated so its possible to keep track of the users and info about them.
                </p>
                
            </div>
        </div>
    </div>
)

export default TipsterBackOffice
