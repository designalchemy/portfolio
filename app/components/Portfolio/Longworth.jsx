import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const Longworth = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Longworth UK</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/longworth/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/longworth/2.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/longworth/3.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/longworth/4.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/longworth/5.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Longworth UK
                </p>

                <p>
                    <strong>URL:</strong>{' '}
                    <a href="http://www.longworth-uk.com/">www.longworth-uk.com/</a>
                </p>

                <p>
                    <strong>Client:</strong> Longworth UK via{' '}
                    <a href="http://theblackeyeproject.co.uk/">The Black Eye Project</a>
                </p>

                <p>
                    <strong>Role:</strong> Sole Developer
                </p>

                <p>
                    <strong>Tech used:</strong> HTML, CSS, JS, Wordpress
                </p>

                <p>
                    <strong>About:</strong> Working for the Black Eye Porject, one of my projects
                    was the Longworth site. Having the design done by the lead designer it was my
                    role to turn this into a full responsive WordPress build.
                </p>

                <p>
                    One of the challanages here was to create all the mobile screens while only the
                    desktop designs were provided, as well as integrating all the animations to give
                    the site a fluid and smooth feel.
                </p>

                <p>
                    The WordPress build also involved ensuring every item within it was editable by
                    the admins, including all the menus, images, text and pages.
                </p>
            </div>
        </div>
    </div>
)

export default Longworth
