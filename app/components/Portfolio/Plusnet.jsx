import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const Plusnet = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Plusnet</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/plusnet/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/plusnet/2.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/plusnet/3.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/plusnet/4.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/plusnet/5.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/plusnet/6.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Plusnet
                </p>

                <p>
                    <strong>URL:</strong> <a href="https://www.plus.net">plus.net</a>
                </p>

                <p>
                    <strong>Role:</strong> Front end development team
                </p>

                <p>
                    <strong>Tech used:</strong> HTML, CSS, JS
                </p>

                <p>
                    <strong>About:</strong> Plusnet was my first commerical developer role, i was in
                    a team of 10 people working on the front end development team, working under a
                    senior designer and support from the senior developers.
                </p>

                <p>
                    The main tasks here were to bug fix, build new pages and preform updates when
                    needed. I also worked closely with the marketing department to provide google
                    analytics reports and preform A/B tests to help improve the sales.
                </p>
            </div>
        </div>
    </div>
)

export default Plusnet
