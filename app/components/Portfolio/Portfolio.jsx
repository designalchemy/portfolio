import React, { Component } from 'react'

import { Link } from 'react-router-dom'

import styles from './Portfolio.scss'

const Portfolio = () => (
    <div className="container">
        <div className="title-container">
            <h3>Portfolio</h3>
            <hr />
        </div>

        <div className="flex-container margin-bottom portfolio-container">
            <Link to="tax-calc">
                <p>Simple Uk Tax</p>
                <img src="/assets/uktax/index.jpg" alt="" />
            </Link>

            <Link to="tipster-display">
                <p>Tipster Display</p>
                <img src="/assets/tipsterDisplay/index.jpg" alt="" />
            </Link>

            <Link to="tipster-back-office">
                <p>Tipster Managment</p>
                <img src="/assets/tipsterBackoffice/index.jpg" alt="" />
            </Link>
        </div>

        <div className="flex-container margin-bottom portfolio-container">
            <Link to="longworth">
                <p>Longworth</p>
                <img src="/assets/longworth/index.jpg" alt="" />
            </Link>

            <Link to="ffrees">
                <p>Ffrees</p>
                <img src="/assets/ffrees/index.jpg" alt="" />
            </Link>

            <Link to="la-fiesta">
                <p>La Fiesta</p>
                <img src="/assets/lafiesta/index.jpg" alt="" />
            </Link>
        </div>

        <div className="flex-container portfolio-container">
            <Link to="my-bet">
                <p>MyBet</p>
                <img src="/assets/mybet/index.jpg" alt="" />
            </Link>

            <Link to="plusnet">
                <p>Plusnet</p>
                <img src="/assets/plusnet/index.jpg" alt="" />
            </Link>

            <Link to="riggotts">
                <p>Riggotts</p>
                <img src="/assets/riggott/index.jpg" alt="" />
            </Link>
        </div>
    </div>
)

export default Portfolio
