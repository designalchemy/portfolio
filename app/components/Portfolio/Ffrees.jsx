import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const Ffrees = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Ffrees</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/ffrees/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/ffrees/2.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/ffrees/3.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/ffrees/4.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Ffrees
                </p>

                <p>
                    <strong>URL:</strong> <a href="https://www.uaccount.uk">www.ffrees.co.uk</a>
                </p>

                <p>
                    <strong>Role:</strong> Front end development team
                </p>

                <p>
                    <strong>Tech used:</strong> HTML, CSS, jQuery, Laravel
                </p>

                <p>
                    <strong>About:</strong> At Ffrees i was part of the front end development team.
                    Our main project was the front end build of a online banking system.
                </p>

                <p>
                    This involved creating HTML pages with AJAX JSON API responses, with a lot of
                    javascript to create a interactive experience. The backend was build in Phalcon
                    PHP and we used a Restful API methodology.
                </p>

                <p>
                    I was working with the lead designer but had a lot of creative freedom in the
                    designing and development of the pages. The team worked in a Agile environment with bi
                    weekly sprints.
                </p>
            </div>
        </div>
    </div>
)

export default Ffrees
