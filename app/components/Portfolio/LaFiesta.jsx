import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const LaFiesta = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>La Fiesta</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/lafiesta/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/lafiesta/2.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/lafiesta/3.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/lafiesta/4.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/lafiesta/5.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/lafiesta/6.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> La Fiesta
                </p>

                <p>
                    <strong>URL:</strong>{' '}
                    <a href="https://www.lafiestadoncaster.uk">www.lafiestadoncaster.co.uk</a>
                </p>

                <p>
                    <strong>Role:</strong> Designer + developer
                </p>

                <p>
                    <strong>Tech used:</strong> HTML, CSS, JS, Wordpress
                </p>

                <p>
                    <strong>About:</strong> La Fiesta was a freelance commision website for a
                    restaurant. I designed and built this website with a graphic designer
                    providing the logo and images.
                </p>

                <p>
                    The site was build in Wordpress from a blank template and is full intergrated
                    with a menu system allowing the resturants menu's to be integrated so they can be
                    visually seen by the customer as well as downloaded.
                </p>

                <p>
                    A custom booking form and contact form was also build into this allowing email
                    bookings and contact.
                </p>
            </div>
        </div>
    </div>
)

export default LaFiesta
