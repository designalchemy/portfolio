import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const MyBet = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>MyBet</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/mybet/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/mybet/2.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/mybet/3.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/mybet/4.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/mybet/5.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> MyBet
                </p>

                <p>
                    <strong>URL:</strong> <a href="https://www.mybet.com">mybet.com</a>
                </p>

                <p>
                    <strong>Client:</strong> MyBet via{' '}
                    <a href="http://www.korelogic.co.uk">Korelogic</a>
                </p>

                <p>
                    <strong>Role:</strong> Front end development team
                </p>

                <p>
                    <strong>Tech used:</strong> React, Mobx, Webpack, HTML, SCSS, JS
                </p>

                <p>
                    <strong>About:</strong> During my time at Korelogic, Mybet was one of the
                    clients we worked for. Mybet is a large sports book serving the European market.
                </p>

                <p>
                    My role with in this project was largely bug fixing, but also responsible for
                    intergrating some new features, widgets and editing the CMS.
                </p>

                <p>
                    The tech stack was react front end with mobx state management. The APIs and data
                    was provided by a 3rd party company and we used a websocket connection to keep
                    all the stats accurate.
                </p>
            </div>
        </div>
    </div>
)

export default MyBet
