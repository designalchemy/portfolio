import React, { Component } from 'react'

import { Link } from 'react-router-dom'

const Carousel = require('react-responsive-carousel').Carousel

const TaxCalc = () => (
    <div className="container ">
        <Link to="/" className="back-arrow">
            {'< Home'}
        </Link>

        <div className="title-container">
            <h3>Simple UK Tax Calcuator</h3>
            <hr />
        </div>

        <div className="flex-container portfolio__sub-container">
            <div className="">
                <Carousel infiniteLoop autoPlay interval={6000}>
                    <div>
                        <img src="/assets/uktax/1.jpg" alt="" />
                    </div>
                    <div>
                        <img src="/assets/uktax/2.jpg" alt="" />
                    </div>
                </Carousel>
            </div>

            <div className="padding">
                <p>
                    <strong>Project:</strong> Simple UK Tax Calcuator
                </p>

                <p>
                    <strong>URL: </strong>
                    <a href="http://www.simple-uk-tax.co.uk">www.simple-uk-tax.co.uk</a>
                </p>

                <p>
                    <strong>Role:</strong> Sole Developer
                </p>

                <p>
                    <strong>Tech used:</strong> React, Redux, Webpack, HTML, SCSS, JS
                </p>

                <p>
                    <strong>About:</strong> Tax calc was a simple build using React and Redux as the
                    main technolgies. The main challange here was to ensure all the rules involving
                    the uk tax laws were being applied correctly at the different threshold.
                </p>

                <p>
                    Built in React with Redux for state mangagement, meaning the render and
                    calucations are super fast allowing for a smooth UI and easy management of data
                    internally.
                </p>
            </div>
        </div>
    </div>
)

export default TaxCalc
