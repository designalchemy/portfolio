import React, { Component } from 'react'

import style from './About.scss'

const About = () => (
    <div>
        <div className="container about-container">
            <div className="title-container">
                <h3>Commercial Experince</h3>
                <hr />
            </div>

            <div className="flex-container margin-bottom">
                <div className="padding-right">
                    <img className="about-image" src="assets/logos/korelogic.jpg" alt="" />

                    <h4>
                        <a href="http://www.korelogic.co.uk">Korelogic</a>
                    </h4>

                    <p>Application Developer</p>

                    <p>1 + Year</p>

                    <p className="small-text">
                        "Full stack" Javascript developer role, responsible for both
                        creating full applications and solutions as well as helping maintain
                        pre-existing applications.
                    </p>

                    <p className="small-text">
                        Largely using React, MobX, Javascript, Express, RethinkDb, Horzion.js and
                        Webpack as the technology stack. Projects range from animated Tipster
                        displaying the day's betting tips to creating a full CMS including the
                        database structure and APIs.
                    </p>

                    <p className="small-text">
                        Working within a 10 person team, that range from shared applications to sole
                        projects. Agile methodology used throughout with Jira and Bitbucket as the
                        software solutions. Responsible for planning releases, liaising with clients
                        and scoping out new projects.
                    </p>
                </div>

                <div className="padding-right">
                    <img className="about-image" src="assets/logos/blackeye.png" alt="" />

                    <h4>
                        <a href="http://www.theblackeyeproject.co.uk">The Black Eye Project</a>
                    </h4>

                    <p>Contract Front End Developer</p>

                    <p>4 Months Contract</p>

                    <p className="small-text">
                        Contract front end developer role, working closely with the lead designers,
                        I would develop full responsive WordPress themes, making it so all aspects
                        were editable.
                    </p>

                    <p className="small-text">
                        Responsible for innovating the technology stack they used and took lead in
                        helping create a new frame work for the front end development to take place
                        in.
                    </p>

                    <p className="small-text">
                        Working within the 10 person company, I was responsible for demoing the site
                        to the clients and rebuilding any changes they requested.
                    </p>
                </div>
            </div>

            <div className="flex-container ">
                <div className="padding-right">
                    <img className="about-image" src="assets/logos/ffrees.png" alt="" />

                    <h4>
                        <a href="http://www.ffrees.co.uk">Ffrees</a>
                    </h4>

                    <p>Front End Developer</p>

                    <p>2+ Years</p>

                    <p className="small-text">
                        Lead front end developer within a 10 person development team, main
                        responsibilities were creating front end pages to be integrated into PHP
                        framework, prototyping new concepts and planning new sections of the site.
                    </p>

                    <p className="small-text">
                        Here I also worked a lot with Analytics to provide marketing detailed
                        information about customers, their activities and website usage.
                    </p>

                    <p className="small-text">
                        A lot of time spent researching new technologies and more interactive
                        methods of customer engagement.
                    </p>
                </div>

                <div>
                    <img className="about-image" src="assets/logos/plusnet.png" alt="" />

                    <h4>
                        <a href="http://www.plus.net">Plusnet</a>
                    </h4>

                    <p>Web Developer</p>

                    <p>2+ Years</p>

                    <p className="small-text">
                        Varied front end role ranging from building web pages, making banners, asset
                        creation to bug fixing. Part of a 150 member development team, with my team
                        being 15 people.
                    </p>

                    <p className="small-text">
                        Working closely with marketing to provide A/B testing, this was done to
                        monitor and test sales with different variants.
                    </p>

                    <p className="small-text">
                        Extensive work done on making legacy browsers such as IE6 work while still
                        maintaining modern features for the latest browsers.
                    </p>
                </div>
            </div>
        </div>

        <div className="container">
            <div className="title-container">
                <h3>Skills</h3>
                <hr />
            </div>

            <div className="flex-container">
                <div>
                    <h4>Expert</h4>

                    <p>HTML</p>

                    <p>CSS/SCSS</p>

                    <p>Javascript</p>

                    <p>React</p>

                    <p>Responsive Development</p>

                    <p>jQuery / Lodash</p>

                    <p>Prototyping</p>
                </div>

                <div>
                    <h4>Adept</h4>

                    <p>MobX</p>

                    <p>Express</p>

                    <p>Rethink / Mongo</p>

                    <p>RESTful API's</p>

                    <p>WordPress</p>

                    <p>Node / Webpack</p>

                    <p>Vue</p>
                </div>

                <div>
                    <h4>Proficient</h4>

                    <p>Redux</p>

                    <p>Unit Testing</p>

                    <p>Angular</p>

                    <p>Functional Programming</p>

                    <p>Laravel</p>

                    <p>Gulp / Grunt</p>

                    <p>Angular</p>
                </div>
            </div>
        </div>
    </div>
)

export default About
