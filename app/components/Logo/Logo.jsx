import React, { Component } from 'react'
import { observable } from 'mobx'
import { observer } from 'mobx-react'
import style from './Logo.scss'

@observer
class Logo extends React.Component {
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll)
    }

    @observable windowPos = 0

    handleScroll = () => {
        this.windowPos = document.documentElement.scrollTop
    }

    render() {
        const transition = this.windowPos > 0 ? 'none' : 'initial'

        return (
            <div className="master-container">
                <div>
                    <div
                        className="logo-container"
                        style={{
                            transform: `rotate(${this.windowPos / 3}deg)`,
                            transition
                        }}
                    >
                        <div className="line-1" />
                        <div className="line-2" />
                        <div className="line-3" />
                        <div className="line-4" />

                        <div className="sub-logo-container">
                            <div className="line-5" />
                            <div className="line-6" />
                            <div className="line-7" />
                            <div className="line-8" />

                            <div className="square" />
                        </div>

                        <div className="circle">
                            <div className="circle" />
                        </div>

                        <div className="square" />
                    </div>

                    <div>
                        <h1>Design Alchemy</h1>
                        <hr className="ani" />
                        <h2>Web Application and Solution Development</h2>
                    </div>
                </div>
            </div>
        )
    }
}

export default Logo
