import React, { Component } from 'react'
import Logo from './components/Logo/Logo'
import About from './components/About/About'
import Portfolio from './components/Portfolio/Portfolio'
import Contact from './components/Contact/Contact'

import style from './App.scss'
import styles from '../node_modules/react-responsive-carousel/lib/styles/carousel.min.css'

const App = () => (
    <div className="full-height">
        <Logo />
        <About />
        <Portfolio />
        <Contact />
    </div>
)

export default App
