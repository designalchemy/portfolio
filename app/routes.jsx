import React, { Component } from 'react'

import _ from 'lodash'

import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Provider } from 'mobx-react'

import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6

import store from './stores'

import App from './App'
import TaxCalc from './components/Portfolio/TaxCalc'
import TipsterDisplay from './components/Portfolio/TipsterDisplay'
import TipsterBackOffice from './components/Portfolio/TipsterBackOffice'
import Longworth from './components/Portfolio/Longworth'
import Ffrees from './components/Portfolio/Ffrees'
import LaFiesta from './components/Portfolio/LaFiesta'
import MyBet from './components/Portfolio/MyBet'
import Plusnet from './components/Portfolio/Plusnet'
import Riggotts from './components/Portfolio/Riggotts'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    routing: routingStore,
    global: store
}

const history = syncHistoryWithStore(browserHistory, routingStore)

class Routes extends Component {
    render() {
        return (
            <Provider {...stores}>
                <Router history={history}>
                    <Route
                        render={({ location, history, match }) => (
                            <div className="full-height">
                                <ReactCSSTransitionGroup
                                    transitionName="route"
                                    transitionAppear={true}
                                    transitionAppearTimeout={1000}
                                    transitionEnter={true}
                                    transitionEnterTimeout={1000}
                                    transitionLeave={true}
                                    transitionLeaveTimeout={1000}
                                >
                                    <Switch key={location.key} location={location}>
                                        <Route exact path="/" component={App} />
                                        <Route exact path="/tax-calc" component={TaxCalc} />
                                        <Route exact path="/tipster-display" component={TipsterDisplay} />
                                        <Route exact path="/tipster-back-office" component={TipsterBackOffice} />
                                        <Route exact path="/longworth" component={Longworth} />
                                        <Route exact path="/ffrees" component={Ffrees} />
                                        <Route exact path="/la-fiesta" component={LaFiesta} />
                                        <Route exact path="/my-bet" component={MyBet} />
                                        <Route exact path="/plusnet" component={Plusnet} />
                                        <Route exact path="/riggotts" component={Riggotts} />

                                        <Route component={App} /> {/* not found route */}
                                    </Switch>
                                </ReactCSSTransitionGroup>
                            </div>
                        )}
                    />
                </Router>
            </Provider>
        )
    }
}

export default Routes
